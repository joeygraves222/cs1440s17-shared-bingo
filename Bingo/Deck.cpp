//
// Created by Stephen Clyde on 2/16/17.
//

#include "Deck.h"

Deck::Deck(int cardSize, int cardCount, int numberMax)
{
    // TODO: Implement
    for (int i = 0; i < cardCount; i++)
    {
        Card* card = new Card(cardSize, numberMax);
        CardDeck.push_back(card);
    }
}

Deck::~Deck()
{
    // TODO: Implement
    for (int i = 0; i < CardDeck.size(); i++)
    {

        CardDeck[i]->~Card();

    }

}

void Deck::print(std::ostream& out)
{
    if (out.rdbuf() !=  cout.rdbuf())
    {
        for (int i = 0; i < CardDeck.size(); i++) {
            out << "Card #: " << i + 1 << endl;
            CardDeck[i]->SaveToFile(out);
            out << endl;
        }
    }
    else
    {
        // TODO: Implement
        for (int i = 0; i < CardDeck.size(); i++) {
            cout << "Card #: " << i + 1 << endl;
            CardDeck[i]->Print();
            cout << endl;
        }
    }

}

void Deck::print(std::ostream& out, int cardIndex)
{
    // TODO: Implement
    cout << "Card #: " << cardIndex + 1 << endl;
    CardDeck[cardIndex]->Print();
    cout << endl;
}



