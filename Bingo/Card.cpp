//
// Created by Joseph Graves on 3/2/17.
//

#include "Card.h"
#include <iomanip>
#include <fstream>
#include <iostream>
using namespace std;


Card::Card(int size, int max)
{
    this->size = size;
    this->max = max;
    this->min = 1;

    //make a vector of numbers to use in the card.
    vector<int> values;
    for (int i = 1; i <= max; i++)
    {
        values.push_back(i);
    }

    // shuffle vector
    random_shuffle(values.begin(), values.end());
    this->numbers = new int*[size];
    for (int i = 0; i < size; i++)
    {
        numbers[i] = new int[size];
    }
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            //numbers[i][j] = new int();
            numbers[i][j] = values.back();
            values.pop_back();
        }
    }
}

Card::~Card()
{
    for (int i = 0; i < size; i++)
    {
        delete numbers[i];
        numbers[i] = NULL;
    }
}

void Card::Print()
{
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            cout << "+----";
        }
        cout << '+' << endl;
        for (int j = 0; j < size; j++)
        {
            if (numbers[i][j] < 10)
            {
                cout << "| 0" << numbers[i][j] << " ";
            }
            else
            {
                cout << "| " << numbers[i][j] << " ";
            }
        }
        cout << "|" << endl;
    }
    for (int j = 0; j < size; j++)
    {
        cout << "+----";
    }
    cout << '+' << endl;

}

void Card::SaveToFile(ostream& fOut)
{
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            fOut << "+----";
        }
        fOut << '+' << endl;
        for (int j = 0; j < size; j++)
        {
            if (numbers[i][j] < 10)
            {
                fOut << "| 0" << numbers[i][j] << " ";
            }
            else
            {
                fOut << "| " << numbers[i][j] << " ";
            }
        }
        fOut << "|" << endl;
    }
    for (int j = 0; j < size; j++)
    {
        fOut << "+----";
    }
    fOut << '+' << endl;

}