//
// Created by Joseph Graves on 3/2/17.
//

#ifndef BINGO_CARD_H
#define BINGO_CARD_H

#include <vector>

using namespace std;

class Card {
public:
    int size, min, max;
    int** numbers;
    Card(int size, int max);
    ~Card();
    void Print();
    void SaveToFile(ostream& out);
};


#endif //BINGO_CARD_H
