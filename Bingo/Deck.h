//
// Created by Stephen Clyde on 2/16/17.
//

#ifndef BINGO_DECK_H
#define BINGO_DECK_H

#include <ostream>
#include <iostream>
#include "Card.h"
#include <vector>

using namespace std;
// TODO: Extend this definition as you see fit

class Deck {

public:
    Deck(int cardSize, int cardCount, int numberMax);
    ~Deck();
    vector<Card*> CardDeck;

    void print(std::ostream& out);
    void print(std::ostream& out, int cardIndex);
};

#endif //BINGO_DECK_H
